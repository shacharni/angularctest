import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { throwMatDialogContentAlreadyAttachedError } from '@angular/material';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class BooksService {


  getBook(id:string,userId:string):Observable<any>{
         return this.db.doc(`users/${userId}/books/${id}`).get();
         }
  
  constructor(private db:AngularFirestore) { }
  bookCollection:AngularFirestoreCollection=this.db.collection('books');
  userCollection:AngularFirestoreCollection=this.db.collection('users');
  addbook(userId:string,title_input:string,author_input:string)
  {
  //  console.log('yes');
    const book={title:title_input,author:author_input};
    this.userCollection.doc(userId).collection('books').add(book);
  }
  getBooks(userId:string):Observable<any[]>{
       // return this.postCollection.valueChanges({idField:'id'}); 
         this.bookCollection= this.db.collection(`users/${userId}/books`);
         return this.bookCollection.snapshotChanges().pipe(
           //pipe מאפשר לשרשר פונקציות
           map(
             //map גם מאפשר לשרשר פונקציות
             collection => collection.map(
               document => {
                 const data = document.payload.doc.data();
                 data.id= document.payload.doc.id;
                 console.log(data);
                 return data;
               }
             )
           )
         )
              }

    upDateBook(userId:string,id:string,title_input:string, author_input:string){
      this.db.doc(`users/${userId}/books/${id}`).update({title:title_input, author:author_input});}
    
    
      deleteBook(id:string,userId:string){
            this.db.doc(`users/${userId}/books/${id}`).delete();
      }    

       
 
  }
  
  
  
  


