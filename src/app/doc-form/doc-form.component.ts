import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-doc-form',
  templateUrl: './doc-form.component.html',
  styleUrls: ['./doc-form.component.css']
})
export class DocFormComponent implements OnInit {

  text:string;

  constructor(private classifyService:ClassifyService, private router:Router) { }

  ngOnInit() {
  }

  onSubmit()
  {
        this.classifyService.doc=this.text;
        console.log(this.text);
        this.router.navigate(['/classified']);
      }

}
