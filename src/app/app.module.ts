
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatExpansionModule} from '@angular/material/expansion';
import { AppComponent } from './app.component';
import { BooksComponent } from './books/books.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AddBookComponent } from './add-book/add-book.component';
import { RouterModule, Routes } from '@angular/router';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import {MatCardModule} from '@angular/material/card';
import { FormsModule }  from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule} from '@angular/fire/firestore';
import { environment } from '../environments/environment';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LoginComponent } from './login/login.component';
import { TemperatureComponent } from './temperature/temperature.component';
import { TempformComponent } from './tempform/tempform.component';
import { HttpClientModule } from '@angular/common/http';
import { ClassifiedComponent } from './classified/classified.component';
import { DocFormComponent } from './doc-form/doc-form.component';
import { PostsComponent } from './posts/posts.component';





const appRoutes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'addbook', component: AddBookComponent },
  { path: 'addbook/:id', component: AddBookComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'login', component: LoginComponent },
  //{ path: 'temperature/:temperature/:city', component: TemperatureComponent },
  { path: 'temperature/:city', component: TemperatureComponent },
  { path: 'tempform', component: TempformComponent },
  { path: 'classified', component:ClassifiedComponent },
  { path: 'docform', component: DocFormComponent },
  { path: 'posts', component: PostsComponent },
  { path: "",
  redirectTo: '/temperature/London',
  pathMatch: 'full'
},
];


@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    NavComponent,
    AddBookComponent,
    SignUpComponent,
    LoginComponent,
    TemperatureComponent,
    TempformComponent,
    ClassifiedComponent,
    DocFormComponent,
    PostsComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatCardModule,
    FormsModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireModule,

    RouterModule.forRoot(
            appRoutes,
           { enableTracing: true } // <-- debugging purposes only
          )    
      
  ],
  providers: [AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
