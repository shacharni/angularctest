import { map,catchError } from 'rxjs/operators';
import { WeatherRaw } from './interfaces/weather-raw';
import { Observable , throwError} from 'rxjs';
import { HttpClient , HttpErrorResponse} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Weather } from './interfaces/weather';




@Injectable({
  providedIn: 'root'
})
export class TempService {
  private URL="http://api.openweathermap.org/data/2.5/weather?q=";
  private KEY="4330dc3a6297d6ac16aa7b68851f3cf8";
  private IMP="&units=metric";

  searchWeatherData(cityName:String):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
    .pipe(map(data=>this.transformWeatherData(data)),
    catchError(this.handleError)
    )
  }
  private handleError(res:HttpErrorResponse){
    console.log("in the service"+res.error);
    return throwError(res.error)
  }
  private transformWeatherData(data:WeatherRaw):Weather{
    return{
      name:data.name,
      country:data.sys.country,
      image:`http://api.openweathermap.org/img/w/${data.weather[0].icon}`,
      description:data.weather[0].description,
      temperature:data.main.temp,
      lat:data.coord.lat,
      lon:data.coord.lon
    }

  }

  constructor(private http:HttpClient) { }
}

