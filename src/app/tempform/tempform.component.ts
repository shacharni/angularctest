import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tempform',
  templateUrl: './tempform.component.html',
  styleUrls: ['./tempform.component.css']
})
export class TempformComponent implements OnInit {
  city:string;
  temperature:number;
  cities:object[]=[{id:1, name:'Tel Aviv'},{id:2, name:'London'},{id:3, name:'Paris'}]

  constructor(public router:Router) { }

  ngOnInit() {
  }
onSubmit(){
 // this.router.navigate(['/temperature', this.temperature,this.city]);
 this.router.navigate(['/temperature',this.city]);
}
}
