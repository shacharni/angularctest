import { ImageService } from './../images.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {
  category:string = "Loading...";
  categoryImage:string; 
  text:string;
  changeCategory:string;
  categories:object = [{name: 'business'}, {name: 'entertainment'},{ name: 'politics'}, {name: 'sport'},{name: 'tech'}];

  constructor(public classifyService:ClassifyService,
              public imageService:ImageService, public router:Router) {}

  ngOnInit() {
    this.classifyService.classify().subscribe(
      res => {
        console.log(res);
        console.log(this.classifyService.categories[res])
        this.category = this.classifyService.categories[res];
        console.log(this.imageService.images[res]);
        this.categoryImage = this.imageService.images[res];
       console.log(this.classifyService.doc)
      }
    )
  }
  change(){
    this.category=this.changeCategory;
  }
  onclick(){
   
    console.log(this.category,this.classifyService.doc)
    this.classifyService.addDoc(this.category,this.classifyService.doc);
    this.router.navigate(['/docform']);
  }
  
}