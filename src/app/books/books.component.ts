import { AuthService } from './../auth.service';
import { Observable } from 'rxjs';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  books:Observable<any>;
  userId:string;

  constructor(public router:Router, public bookService:BooksService, private route:ActivatedRoute,public authService:AuthService) { }

  ngOnInit() {
    this.authService.user.subscribe(
            user => {
              this.userId = user.uid;
              this.books = this.bookService.getBooks(this.userId);        }
         )
  }
    deleteBook(id:string){
      this.bookService.deleteBook(id,this.userId); 
  
    }
    }