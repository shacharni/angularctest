import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {
  //private url = "https://rniqbt3fbh.execute-api.us-east-1.amazonaws.com/beta"
  private url="https://sbk2uzgno5.execute-api.us-east-1.amazonaws.com/beta"
  public categories:object = {0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'};

  public doc:string;

  docCollection:AngularFirestoreCollection=this.db.collection('docs');

  addDoc(subject_input:string,text_input:string)
  {
    
    const docu={subject:subject_input,text:text_input};
    console.log(docu);
    this.docCollection.add(docu);
    //console.log("yes");
  }
  classify():Observable<any>{
   
    let json = {
      "articles":[
        {"text":this.doc}
      ]
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
     map(res=>{
       let final= res.body.replace('[','');
       final= final.replace(']','');
      return final;
     })
    ) 
    
  }

  constructor(private http:HttpClient,private db:AngularFirestore) { }
}
