import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Post } from '../interfaces/post';
import { Users } from '../interfaces/users';
import { UsersService } from '../users.service';
@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  Post$:Post[];
  User$:Users[];
  constructor(private postsservis:PostsService,private usersservice:UsersService) { }

  ngOnInit() {
    this.postsservis.getPost().subscribe(data=>this.Post$=data);
    this.usersservice.getUser().subscribe(data=>this.User$=data);
  }

}
