import { Post } from './interfaces/post';
import { Injectable } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  apiurl="https://jsonplaceholder.typicode.com/posts/";
  constructor(private http:HttpClient) { }

  getPost(){ 
       return this.http.get<Post[]>(this.apiurl);
      }
}
