import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { BooksService } from './../books.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {
  title:string;
  author:string;
  id:string;
  buttonText:string="Add a new book";
  isEdit:boolean=false;
  userId:string;

  constructor(public router:Router, public bookService:BooksService, private route:ActivatedRoute, public authService:AuthService) { }

  ngOnInit() {
    this.id=this.route.snapshot.params.id;
    this.authService.user.subscribe
       (
              user => 
              {
                this.userId = user.uid;
                if(this.id){
                  this.isEdit=true;
                  this.buttonText="Update book";
                  this.bookService.getBook(this.id,this.userId).subscribe
                  (
                    post=> 
                    {
                      console.log(post.data().author);
                      this.author=post.data().author;
                      this.title=post.data().title;
                      
                    }
                  )
                 }
              
            }
      )
    }
  
      
 
  onSubmit()
  {
    if(!this.isEdit){
        this.bookService.addbook(this.userId,this.title,this.author)
         }
    
      else{
        this.bookService.upDateBook(this.userId,this.id,this.title,this.author);
        
      }
      this.router.navigate(['/books'])
      }
      
      

    
   

}
