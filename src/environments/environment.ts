// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig :{
    apiKey: "AIzaSyB3hJ3Xtu1KjPNrugaOJi7WChMWJSCNWmw",
    authDomain: "angularctest.firebaseapp.com",
    databaseURL: "https://angularctest.firebaseio.com",
    projectId: "angularctest",
    storageBucket: "angularctest.appspot.com",
    messagingSenderId: "678628770353",
    appId: "1:678628770353:web:e05ec9712260edc7f34b10",
    measurementId: "G-P547TXZHLW"
  
}
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
